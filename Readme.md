# Terraform AWS basic network foundation

##### Requirements

- terraform 12x
- AWS Credentials configuration
- https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

### Terraform Configuration

https://www.terraform.io/downloads.html

```bash
terraform init
```

### This project will be create

- Basic AWS VPC fundation, with one public subnet.
- Terraform remote state, wich will create a tsfate file in a Bucket S3
